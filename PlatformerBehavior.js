class PlatformerBehavior extends AbstractBehavior{
    constructor(scene, type=PlatformerBehavior.TYPE_UDLR, imageDirection=PlatformerBehavior.DIRECTION_RIGHT,
        velocityXL=-60, velocityXR=60, velocityYU=-300, velocityYD=0, 
        animation_Horizontal=null, animation_Up=null, animation_Down=null){
        super(scene);
        this.type=type;
        this.imageDirection=imageDirection;
        this.velocityXL=velocityXL;
        this.velocityXR=velocityXR;
        this.velocityYU=velocityYU;
        this.velocityYD=velocityYD;
        this.animation_Horizontal=animation_Horizontal;
        this.animation_Up=animation_Up;
        this.animation_Down=animation_Down;
    }
    static TYPE_WSAD="WSAD";
    static TYPE_UDLR="UDLR";
    static DIRECTION_RIGHT="DIRECTION_RIGHT";
    static DIRECTION_LEFT="DIRECTION_LEFT";

    setType(type){
        this.type=type;
    }

    setImageDirection(imageDirection){
        this.imageDirection=imageDirection;
    }

    setVelocityXL(velocityXL){
        this.velocityXL=velocityXL;
    }

    setVelocityXR(velocityXR){
        this.velocityXR=velocityXR;
    }

    setVelocityYU(velocityYU){
        this.velocityYU=velocityYU;
    }

    setVelocityYD(velocityYD){
        this.velocityYD=velocityYD;
    }

    setAnimation_Horizontal(animation_Horizontal){
        this.animation_Horizontal=animation_Horizontal;
    }

    setAnimation_Up(animation_Up){
        this.animation_Up=animation_Up;
    }

    setAnimation_Down(animation_Down){
        this.animation_Down=animation_Down;
    }

    apply(sprite){
        sprite.setVelocityX(0);
        //this.player.setVelocityY(0);
        if (this.scene.cursors.left.isDown) {
            sprite.flipX = (this.imageDirection==PlatformerBehavior.DIRECTION_RIGHT);
            sprite.setVelocityX(this.velocityXL);
            sprite.anims.play(this.animation_Horizontal, true);
        } else if (this.scene.cursors.right.isDown) {
            sprite.flipX = (this.imageDirection==PlatformerBehavior.DIRECTION_LEFT);
            sprite.setVelocityX(this.velocityXR);
            sprite.anims.play(this.animation_Horizontal, true);
        }  
        if(this.scene.cursors.up.isDown && (sprite.body.blocked.down || sprite.body.touching.down)) {
            sprite.setVelocityY(this.velocityYU);
            sprite.anims.play(this.animation_Up, true);
        }
    }
}