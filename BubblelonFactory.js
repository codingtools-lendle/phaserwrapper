class BubblelonFactory extends SpriteFactory{
    constructor(scene){
        super(scene);
        this.scene=scene;
    }
    createFullName(localName){
        return localName;
    }
    
    preload(){
        this.loadSpritesheet("bubblelon", "assets/bubblelon.png", 16, 16);
    }

    postload(){
        this.createAnimation("up","bubblelon", 0,0,10,-1);
        this.createAnimation("down","bubblelon", 0,1,10,-1);
        this.createAnimation("side","bubblelon", 0,4,10,-1);
        this.createAnimation("shoot","bubblelon", 5,7,10,-1);
    }

    createInstance(x, y){
        return new Player(this.scene, x, y, "bubblelon");
    }
}