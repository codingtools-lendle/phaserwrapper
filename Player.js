class Player extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y){
        super(scene, x, y, "bubblelon");
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
    }
}