class WhaleFactory extends SpriteFactory{
    constructor(scene){
        super(scene);
        this.scene=scene;
    }
    createFullName(localName){
        return localName;
    }
    preload(){
        this.loadSpritesheet("whale", "assets/whale.png", 16, 16);
    }

    postload(){
        this.createAnimation("whale", "whale", 0, 2, 5, -1);
        this.createAnimation("whale_bubble", "whale", 3, 5, 5, -1);
    }

    createInstance(x, y){
        return new Whale(this.scene, x, y, "whale");
    }
}