class ZenChanFactory extends SpriteFactory{
    constructor(scene){
        super(scene);
        this.scene=scene;
    }
    createFullName(localName){
        return localName;
    }
    preload(){
        this.loadSpritesheet("zenchan", "assets/zenchan.png", 16, 16);
    }

    postload(){
        this.createAnimation("zenchan", "zenchan", 0, 3, 5, -1);
        this.createAnimation("zenchan_bubble", "zenchan", 4, 6, 5, -1);
    }

    createInstance(x, y){
        return new ZenChan(this.scene, x, y, "zenchan");
    }
}