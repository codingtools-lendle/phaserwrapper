class TileFactory extends ObjectFactory{
    constructor(scene){
        super(scene);
        this.image2KeyMap={};
        this.json2KeyMap={};
    }

    loadTiledMapJSON(localName, jsonFile, tilesetImages, objectIds2Images){
        let fullName=localName;
        this.scene.load.json(fullName, jsonFile);
        this.json2KeyMap[jsonFile]=fullName;
        this.scene.load.tilemapTiledJSON(fullName, jsonFile);
        for(let tilesetImage of tilesetImages){
            let name="tilesetImage"+Date.now();
            this.scene.load.image(name, tilesetImage);
            this.image2KeyMap[tilesetImage]=name; 
        }
        for(let objectId in objectIds2Images){
            let name=""+objectId;
            this.scene.load.image(name, objectIds2Images[objectId]);
            this.image2KeyMap[objectIds2Images[objectId]]=name; 
        }
    }

    createTilemapInstance(mapLocalName){
        let fullName=mapLocalName;
        let map = this.scene.make.tilemap({ key: fullName });
        let tileMapObject=new TileMapObject(map);
        let json=this.scene.cache.json.get(fullName);
        let tileImageProperties={};//tile image name=>{minId, maxId}
        for(let i=0; i<json['tilesets'].length; i++){
            let tileset=json['tilesets'][i];
            let name=tileset['name'];
            let image=tileset['image'];
            let tileImageName=name;
            map.addTilesetImage(name, this.image2KeyMap["assets/"+image]);
            tileImageProperties[tileImageName]={
                minGid: tileset['firstgid'],
                maxGid: (i<json['tilesets'].length-1)?json['tilesets'][i]['firstgid']-1:Number.MAX_SAFE_INTEGER
            };
        }
        for(let layer of json['layers']){
            //determine the correct tile image to use
            if(layer['type']=="tilelayer"){
                let correctTileImageName=null;
                for(let i in layer['data']){
                    let data=layer['data'][i];
                    if(data!=0){
                        for(let tileImageName in tileImageProperties){
                            if(data>=tileImageProperties[tileImageName].minGid && 
                                data<=tileImageProperties[tileImageName].maxGid ){
                                    correctTileImageName=tileImageName;
                                    break;
                            }
                        }
                    }
                    if(correctTileImageName){
                        break;
                    }
                }
                map.createStaticLayer(layer['name'], correctTileImageName, 0, 0);
            }else if(layer['type']=="objectgroup"){
                for(let object of layer['objects']){
                    let objectList=map.createFromObjects(layer['name'], object['gid'], {key: ""+object['gid']});
                    tileMapObject.setObjects(layer['name'], object['gid'], objectList);
                }               
            }
        }
        return tileMapObject;
    }
}

class TileMapObject{
    constructor(phaserTileMap){
        this.phaserTileMap=phaserTileMap;
        this.objectsRegistry={};
    }

    getPhaserTileMap(){
        return this.phaserTileMap;
    }

    getStaticLayer(layerName){
        return this.phaserTileMap.getLayer(layerName).tilemapLayer;
    }

    getObjects(objectLayerName, gid){
        return this.objectsRegistry[objectLayerName+"::"+gid];
    }

    setObjects(objectLayerName, gid, objects){
        this.objectsRegistry[objectLayerName+"::"+gid]=objects;
    }
}