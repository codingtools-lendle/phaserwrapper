class AbstractBehavior{
    constructor(scene){
        this.scene=scene;
    }
    /**
     * apply the behavior to the target sprite
     * @param {*} sprite
     */
    apply(sprite){
        throw new Error("must implement");
    }

    shouldApply(){
        return true;
    }
}