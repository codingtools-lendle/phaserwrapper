/**
 * apply behavior to other objects
 */

class BehaviorController{
    constructor(){
        this.behaviors=new Map();//object=>list of behaviors apply to it
    }

    update(){
        for(let [o, behaviors] of this.behaviors){
            for(let b of behaviors){
                //apply the behavior
                b.apply(o);
            }
        }
    }

    addBehavior(sprite, behavior){
        let behaviorList=this.behaviors.get(sprite);
        if(!behaviorList){
            behaviorList=[];
            //this.behaviors[sprite]=behaviorList;
            this.behaviors.set(sprite, behaviorList);
        }
        behaviorList.push(behavior);
    }
}