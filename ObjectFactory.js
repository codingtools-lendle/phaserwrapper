class ObjectFactory{
    constructor(scene){
        this.scene=scene;
    }

    preload(){
        throw new Error("must implement");
    }

    loadImage(localName, imageSrc){
        this.scene.load.image(localName, imageSrc);
    }
}