class SpriteFactory extends ObjectFactory{
    constructor(scene){
        super(scene);
    }

    postload(){
        throw new Error("must implement");
    }

    createSpriteInstance(x, y){
        throw new Error("must implement");
    }
  
    loadSpritesheet(localName, imageSrc, frameWidth, frameHeight){
        this.scene.load.spritesheet(localName,
            imageSrc,
            { frameWidth: frameWidth, frameHeight: frameHeight }
        );
    }

    createAnimation(localName, imageSrc, start, end, frameRate=20, repeat=-1){
        this.scene.anims.create({
            key: localName,
            frames: this.scene.anims.generateFrameNumbers(imageSrc, { start: start, end: end }),
            frameRate: frameRate,
            repeat: repeat
        });
    }
}