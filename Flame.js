class Flame extends Phaser.Physics.Arcade.Sprite{
    constructor(scene, x, y, vx, vy, fromEnemy){
        super(scene, x, y, "flame");
        this.scene=scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.setVelocityX(vx);
        this.setVelocityY(vy);
        let self=this;
        setTimeout(function(){
            self.disableBody(true, true);
        },300);
    }
}