/**
 * 定義一個 scene，用成員變數儲存 scene 上面的物件
 * override preload, create, update 函式
 */
class Scene1 extends Phaser.Scene {
    constructor() {
        super();
        this.player = null;
        this.platforms = null;
        this.cursors = null;
        this.lastBomb = -1;
        this.whaleFactory=new WhaleFactory(this);
        this.zenChanFactory=new ZenChanFactory(this);
        this.bubblelonFactory=new BubblelonFactory(this);
        this.bubbleFactory=new BubbleFactory(this);
        this.tileFactory=new TileFactory(this, "tile");
    }
    preload() {
        this.whaleFactory.preload();
        this.zenChanFactory.preload();
        this.bubblelonFactory.preload();
        this.bubbleFactory.preload();
        this.tileFactory.loadTiledMapJSON("map", "assets/level1.json", ["assets/tileset.png"], {211: 'assets/tile210.png'});
        // this.load.image('tiles', 'assets/tileset.png');
        // this.load.tilemapTiledJSON('map', 'assets/level1.json');
        // this.load.spritesheet('bubble',
        //     'assets/bubble.png',
        //     { frameWidth: 16, frameHeight: 16 }
        // );
        this.load.image('gate', 'assets/tile210.png');
        this.load.json('level1', 'assets/level1.json');
    }
    create() {
        let self = this;
        this.whaleFactory.postload();
        this.zenChanFactory.postload();
        this.bubblelonFactory.postload();
        this.bubbleFactory.postload();
        // console.log(this.cache.json.get('level1'));
        this.cameras.main.setBounds(0, 0, 800, 1600);
        // const map = this.make.tilemap({ key: 'map' });
        // const tileset = map.addTilesetImage('level1', 'tiles');
        // this.platforms = map.createStaticLayer('static', tileset, 0, 0);
        let map=this.tileFactory.createTilemapInstance("map");
        this.platforms=map.getStaticLayer('static');
        
        map.getPhaserTileMap().setCollisionByExclusion([206]);
        this.map=map;

        this.player = this.bubblelonFactory.createInstance(50, 1580);
        this.cameras.main.startFollow(this.player);
        this.cursors = this.input.keyboard.createCursorKeys();
        this.physics.add.collider(this.platforms, this.player);
        
        this.whales=[];
        for(let i=0; i<10; i++){
            let whale=this.whaleFactory.createInstance(10+Math.random()*750, Math.random()*1600);
            this.whales.push(whale);

        }

        this.zenchans=[];
        for(let i=0; i<10; i++){
            let zenchan=this.zenChanFactory.createInstance(10+Math.random()*750, Math.random()*1600);
            this.zenchans.push(zenchan);
        }

        this.bubbles=[];
        this.physics.add.collider(this.platforms, this.whales);
        this.physics.add.collider(this.platforms, this.zenchans);

        this.behaviorController=new BehaviorController();
        let platformerController=new PlatformerBehavior(this);
        platformerController.setAnimation_Horizontal("side");
        platformerController.setAnimation_Up("up");
        platformerController.setImageDirection(PlatformerBehavior.DIRECTION_LEFT);
        this.behaviorController.addBehavior(this.player, platformerController);
    }

    update() {
        let current = Date.now();
        this.behaviorController.update();
        // this.player.setVelocityX(0);
        //this.player.setVelocityY(0);
        // if (this.cursors.left.isDown) {
        //     this.player.flipX = false;
        //     this.player.setVelocityX(-60);
        //     this.player.anims.play('side', true);
        // } else if (this.cursors.right.isDown) {
        //     this.player.flipX = true;
        //     this.player.setVelocityX(60);
        //     this.player.anims.play('side', true);
        // }  
        // if(this.cursors.up.isDown && this.player.body.blocked.down) {
        //     this.player.setVelocityY(-250);
        //     this.player.anims.play('up', true);
        // }
        if(this.cursors.space.isDown){
            this.player.anims.play('shoot', true);
            let b=new Bubble(this, this.player.x, this.player.y);
            this.bubbles.push(b);
            b.setVelocityX((this.player.flipX)?50:-50);
            b.body.setAllowGravity(false);
            b.setVelocityY(-1);
        }

        for(let whale of this.whales){
            whale.update();
        }

        for(let zenchan of this.zenchans){
            zenchan.update();
        }
    }
}