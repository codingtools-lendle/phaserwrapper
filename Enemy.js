class Enemy extends Phaser.Physics.Arcade.Sprite {
    constructor(scene, x, y) {
        super(scene, x, y, "Enemy");
        this.scene = scene;
        scene.physics.world.enable(this);
        scene.add.existing(this);
        this.lastFire=-1;
        this.targetPath=null;
    }

    fire() {
        let current=Date.now();
        if((current-this.lastFire)<500){
            return;
        }else{
            this.lastFire=current;
        }
        let self = this;
        let x = this.x;
        let y = this.y;
        self.scene.flames.push(new Flame(self.scene, x, y, -30, 0, true));
        self.scene.flames.push(new Flame(self.scene, x, y , 30, 0, true));
        self.scene.flames.push(new Flame(self.scene, x, y , 0, -30, true));
        self.scene.flames.push(new Flame(self.scene, x, y , 0, 30, true));
        self.scene.flames.push(new Flame(self.scene, x, y , -30, -30, true));
        self.scene.flames.push(new Flame(self.scene, x, y , 30, 30, true));
        self.scene.flames.push(new Flame(self.scene, x , y , 30, -30, true));
        self.scene.flames.push(new Flame(self.scene, x, y , -30, 30, true));
    }
    update() {
        this.setVelocityX(0);
        this.setVelocityY(0);
        if(this.targetPath!=null && this.targetPath.length>0){
            let _target=this.targetPath[0];
            let target={
                x: _target.x*16+8,
                y: _target.y*16+8,
            };
            //console.log(target);
            if(Phaser.Math.Distance.Between(this.x, this.y, target.x, target.y)<2){
                this.targetPath.shift();
            }else{
                if(this.x>target.x){
                    this.setVelocityX(-30);
                }else{
                    this.setVelocityX(30);
                }

                if(this.y>target.y){
                    this.setVelocityY(-30);
                }else{
                    this.setVelocityY(30);
                }
            }
        }
    }
}