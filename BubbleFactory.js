class BubbleFactory extends SpriteFactory{
    constructor(scene){
        super(scene);
        this.scene=scene;
    }
    createFullName(localName){
        return localName;
    }
    
    preload(){
        this.loadSpritesheet("bubble", "assets/bubble.png", 16, 16);
    }

    postload(){
        this.createAnimation("bubble","bubble", 0,6,5,-1);
    }

    createInstance(x, y){
        return new Bubble(this.scene, x, y, "bubble");
    }
}